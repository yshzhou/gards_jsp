<%@ page contentType="text/html;charset=UTF-8"%>    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ include file="inc.jsp"%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trace</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style>
body { padding:10px; }
body,th,td { font-size: 12px; }
    </style>
    <script src="jquery.min.js"></script>
	<script src="http://api.map.baidu.com/api?v=2.0&ak=26cxRIZuRqVLDBfbuWUCyfiF"></script>
    <script src="http://developer.baidu.com/map/jsdemo/demo/changeMore.js"></script>
    <script>
var BAIDU_TRANSMORE_COUNT=20;//参见changeMore.js源码，每次转换20个
var transMore_total=0;//全部需要转换的个数
var transMore_cnt=0;//已经转换了几次

var i = 0;
var aid = cid = '';
var mapHis;
var pointArray = [];
$(document).ready(function(){
    var h = $(document).height() - 94;
    $("#maph").height(h);
    $(window).resize(function(){
        h = $(document).height() - 94;
        $("#maph").height(h);
    });

    $("#adaptor").change(function(){
        pointArray = [];
        aid = $("#adaptor").val();
        if(aid==""){
            $("#maph").empty();
        }else{
            cid = $("#adaptor").find("option:selected").attr("cid");
            $("#maph").empty();
            $.ajax({
	            type: "GET",
	            url: "viewReq1.jsp",
	            data: {
	                aid:aid,
                    cid:cid
	            },
                dataType: "json",
	            success: function(data){
				    if(data.maph){
                        $(".his").show();
                        var maphx = new Array();
                        var maphy = new Array();
                        var count = 0;
                        for(i=0;i<data.maph.length;i++){
                            var maph = new Array();
                            maph = data.maph[i].data.split(",");
                            if((Number(maph[0])!=0)&&(Number(maph[0])!=0)){
                                maphx[count] = Number(maph[0]);
                                maphy[count] = Number(maph[1]);
                                count++;
                            }
                        }
                        if(count!=0){
                            mapHis = new BMap.Map("maph");
                            var _pointArray = [];
                            for(i=0;i<count;i++){
                                _pointArray.push(new BMap.Point(maphx[i],maphy[i]));
                            }
                            transMore_total = count;
                            transMore_cnt = 0;
                            BMap.Convertor.transMore(_pointArray, 0, callback);
                        }
                    }
	            }
            });
        }
    });
});
function callback(xyResults){
	transMore_cnt+=1;
    var xyResult = null;
    for(var index in xyResults){
	    xyResult = xyResults[index];
	    if(xyResult.error != 0){continue;}//出错就直接返回;
	    var point = new BMap.Point(xyResult.x, xyResult.y);
        pointArray.push(point);
    }
	if (Math.ceil(transMore_total/BAIDU_TRANSMORE_COUNT) == transMore_cnt){
		displayTrace();
	}
}
function displayTrace(){
	if(pointArray.length > 1)
	{
        mapHis.centerAndZoom(pointArray[0], 15);
        var icons = new BMap.Icon("start.png", new BMap.Size(29,32));
        var markers = new BMap.Marker(pointArray[0],{icon:icons});
        mapHis.addOverlay(markers);
        var icone = new BMap.Icon("end.png", new BMap.Size(29,32));
        var markere = new BMap.Marker(pointArray[pointArray.length-1],{icon:icone});
        mapHis.addOverlay(markere);
        var polyline = new BMap.Polyline(pointArray,{strokeColor:"red", strokeWeight:2, strokeOpacity:0.5});
        mapHis.addOverlay(polyline);
        var nc = new BMap.NavigationControl();
        mapHis.addControl(nc);
	}
}
    </script>
</head>
<body>
    <div class="input-group">
        <span class="input-group-addon">适配器</span>
        <select id="adaptor" class="form-control">
            <option value="">请选择适配器</option>
<%
String spl = "SELECT id,adaptor_id,category_id FROM gards_adaptor where applied=1";
ResultSet rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("id")%>" cid="<%=rs.getString("category_id")%>"><%=rs.getString("adaptor_id")%></option>
<%}
%>
        </select>
    </div>
    <div class="main">
        <div class="his">
            <h4>历史轨迹</h4>
            <div id="maph"></div>
        </div>
    </div>
</body>
</html>
