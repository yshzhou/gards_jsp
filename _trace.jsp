<%@ page contentType="text/html;charset=UTF-8"%>    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ include file="inc.jsp"%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trace</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style>
body { padding:10px; }
body,th,td { font-size: 12px; }
.main { display: none; }
    </style>
    <script src="jquery.min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=26cxRIZuRqVLDBfbuWUCyfiF"></script>
<script>
//2011-7-25
(function(){        //闭包
function load_script(xyUrl, callback){
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = xyUrl;
    //借鉴了jQuery的script跨域方法
    script.onload = script.onreadystatechange = function(){
        if((!this.readyState || this.readyState === "loaded" || this.readyState === "complete")){
            callback && callback();
            // Handle memory leak in IE
            script.onload = script.onreadystatechange = null;
            if ( head && script.parentNode ) {
                head.removeChild( script );
            }
        }
    };
    // Use insertBefore instead of appendChild  to circumvent an IE6 bug.
    head.insertBefore( script, head.firstChild );
}
function translate(point,type,callback){
    var callbackName = 'cbk_' + Math.round(Math.random() * 10000);    //随机函数名
    var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from="+ type + "&to=4&x=" + point.lng + "&y=" + point.lat + "&callback=BMap.Convertor." + callbackName;
    //动态创建script标签
    load_script(xyUrl);
    BMap.Convertor[callbackName] = function(xyResult){
        delete BMap.Convertor[callbackName];    //调用完需要删除改函数
        var point = new BMap.Point(xyResult.x, xyResult.y);
        callback && callback(point);
    }
    return point;
}
window.BMap = window.BMap || {};
BMap.Convertor = {};
BMap.Convertor.translate = translate;
})();
</script>
    <script>
var i = 0;
var aid = cid = '';
function callback(point){}
$(document).ready(function(){
    var h = $(document).height() - 94;
    $("#maph").height(h);
    $(window).resize(function(){
        h = $(document).height() - 94;
        $("#maph").height(h);
    });
    $("#adaptor").change(function(){
        aid = $("#adaptor").val();
        if(aid==""){
            $(".main").hide();
        }else{
            cid = $("#adaptor").find("option:selected").attr("cid");
            $("#maph").empty();
            $(".main").show();
            $.ajax({
	            type: "POST",
	            url: "viewReq1.jsp",
	            data: {
	                aid:aid,
                    cid:cid
	            },
                dataType: "json",
	            success: function(data){
				    if(!data.maph){
                        $(".his").hide();
                    }else{
                        $(".his").show();
                        var maphx = new Array();
                        var maphy = new Array();
                        var count = 0;
                        for(i=0;i<data.maph.length;i++){
                            var maph = new Array();
                            maph = data.maph[i].data.split(",");
                            if((Number(maph[0])!=0)&&(Number(maph[0])!=0)){
                                maphx[count] = Number(maph[0]);
                                maphy[count] = Number(maph[1]);
                                count++;
                            }
                        }
                        if(count==0){
                            $(".his").hide();
                        }else{
                            var maph = new BMap.Map("maph");
                            var _pointArray = new Array();
                            var pointArray = new Array();
                            for(i=0;i<count;i++){
                                _pointArray[i] = new BMap.Point(maphx[i],maphy[i]);
                                pointArray[i] = BMap.Convertor.translate(_pointArray[i],0,callback);
                            }
                            maph.centerAndZoom(_pointArray[0], 15);
                            var icons = new BMap.Icon("start.png", new BMap.Size(29,32));
                            var markers = new BMap.Marker(_pointArray[0],{icon:icons});
                            maph.addOverlay(markers);
                            var icone = new BMap.Icon("end.png", new BMap.Size(29,32));
                            var markere = new BMap.Marker(_pointArray[count-1],{icon:icone});
                            maph.addOverlay(markere);
                            var polyline = new BMap.Polyline(_pointArray,{strokeColor:"red", strokeWeight:2, strokeOpacity:0.5});
                            maph.addOverlay(polyline);
                            var nc = new BMap.NavigationControl();
                            maph.addControl(nc);
                        }
                    }
	            }
            });
        }
    });
});
    </script>
</head>
<body>
    <div class="input-group">
        <span class="input-group-addon">适配器</span>
        <select id="adaptor" class="form-control">
            <option value="">请选择适配器</option>
<%
String spl = "SELECT id,adaptor_id,category_id FROM gards_adaptor where applied=1";
ResultSet rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("id")%>" cid="<%=rs.getString("category_id")%>"><%=rs.getString("adaptor_id")%></option>
<%}
%>
        </select>
    </div>
    <div class="main">
        <div class="his">
            <h4>历史轨迹</h4>
            <div id="maph"></div>
        </div>
    </div>
</body>
</html>
