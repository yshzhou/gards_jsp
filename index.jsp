<%@ page contentType="text/html;charset=UTF-8"%>    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ include file="inc.jsp"%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gards</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style>
body { padding: 0 10px; }
.input-group { margin-top: 10px; }
.en { display:none; }
.buttons {
    padding: 10px 0;
    text-align: right;
}
    </style>
    <script src="jquery.min.js"></script>
    <script>
var i = 0;
var aid = "";
$(document).ready(function(){
    $(".input-group").hide();
    $("h4").hide();
    $(".input-group").eq(0).show();
    $(".input-group").eq(1).show();
    $(".input-group").eq(2).show();
    $(".input-group").eq(3).show();
    var cid = "";
    $("select").change(function(){
        cid = $(this).val();
        aid = $(this).find("option:selected").attr("aid");
        if(cid==""){
            $(".input-group").eq(1).find("input[type='text']").val("");
            $("h4").hide();
            for(i=4;i<$(".input-group").size();i++){
                $(".input-group").eq(i).hide();
            }
        }else{
            $(".input-group").eq(1).find("input[type='text']").val($(this).find("option:selected").attr("cname"));
            $("h4").show();
            for(i=4;i<8;i++){
                $(".input-group").eq(i).show();
            }
            for(i=8;i<$(".input-group").size();i++){
                if($(".input-group").eq(i).attr("cid")==cid){
                    $(".input-group").eq(i).show();
                }else{
                    $(".input-group").eq(i).hide();
                }
            }
        }
        update();
    });
    $(".btn-danger").click(function(){
        for(i=8;i<$(".input-group").size();i++){
            $(".input-group").eq(i).find("input[type='text']").val("");
        }
    });
    $(".btn-primary").click(function(){
        var version = $(".input-group").eq(2).find("input[type='text']").val();
        if(cid==""){
            alert("请选择适配器");
            return;
        }else if(version==""){
            alert("请输入版本号");
            return;
        }else{
            var count1 = count2 = count3 = count4 = count5 = 0;
            var array1 = new Array();
            var array2 = new Array();
            var array3 = new Array();
            var array4 = new Array();
            var array5 = new Array();
            for(i=8;i<$(".input-group").size();i++){
                if(($(".input-group").eq(i).attr("cid")==cid)&&($(".input-group").eq(i).attr("cat")=="station")&&($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array1[count1]=i;
                    count1++;
                }
                if(($(".input-group").eq(i).attr("cid")==cid)&&($(".input-group").eq(i).attr("cat")=="event")&&($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array2[count2]=i;
                    count2++;
                }
                if(($(".input-group").eq(i).attr("cid")==cid)&&($(".input-group").eq(i).attr("cat")=="alert")&&($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array3[count3]=i;
                    count3++;
                }
                if(($(".input-group").eq(i).attr("cid")==cid)&&($(".input-group").eq(i).attr("cat")=="set")&&($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array4[count4]=i;
                    count4++;
                }
                if(($(".input-group").eq(i).attr("cid")==cid)&&($(".input-group").eq(i).attr("cat")=="req")&&($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array5[count5]=i;
                    count5++;
                }
            }
            var payload = "{";
            if($("#gis").prop("checked")!=""){
                payload += '"GIS":"';
                for(i=4;i<8;i++){
                    if($(".input-group").eq(i).find("input[type='text']").val()!=''){
                        payload += $(".input-group").eq(i).find("input[type='text']").val();
                    }else{
                        payload += '0';
                    }
                    if(i!=7){
                        payload += ',';
                    }
                }
                payload += '"';
                if((count1!=0)||(count2!=0)||(count3!=0)||(count4!=0)||(count5!=0)){
                    payload += ',';
                }
            }
            if(count1!=0){
                payload += '"STA":{';
                for(i=0;i<count1;i++){
                    payload += '"';
                    payload += $(".input-group").eq(array1[i]).find(".en").html();
                    payload += '":"';
                    payload += $(".input-group").eq(array1[i]).find("input[type='text']").val();
                    payload += '"';
                    if(i!=count1-1){
                        payload += ',';
                    }
                }
                payload += '}';
                if((count2!=0)||(count3!=0)||(count4!=0)||(count5!=0)){
                    payload += ',';
                }
            }
            if(count2!=0){
                payload += '"EVNT":{';
                for(i=0;i<count2;i++){
                    payload += '"';
                    payload += $(".input-group").eq(array2[i]).find(".en").html();
                    payload += '":"';
                    payload += $(".input-group").eq(array2[i]).find("input[type='text']").val();
                    payload += '"';
                    if(i!=count2-1){
                        payload += ',';
                    }
                }
                payload += '}';
                if((count3!=0)||(count4!=0)||(count5!=0)){
                    payload += ',';
                }
            }
            if(count3!=0){
                payload += '"ALT":{';
                for(i=0;i<count3;i++){
                    payload += '"';
                    payload += $(".input-group").eq(array3[i]).find(".en").html();
                    payload += '":"';
                    payload += $(".input-group").eq(array3[i]).find("input[type='text']").val();
                    payload += '"';
                    if(i!=count3-1){
                        payload += ',';
                    }
                }
                payload += '}';
                if((count4!=0)||(count5!=0)){
                    payload += ',';
                }
            }
            if(count4!=0){
                payload += '"SET":{';
                for(i=0;i<count4;i++){
                    payload += '"';
                    payload += $(".input-group").eq(array4[i]).find(".en").html();
                    payload += '":"';
                    payload += $(".input-group").eq(array4[i]).find("input[type='text']").val();
                    payload += '"';
                    if(i!=count4-1){
                        payload += ',';
                    }
                }
                payload += '}';
                if(count5!=0){
                    payload += ',';
                }
            }
            if(count5!=0){
                payload += '"REQ":{';
                for(i=0;i<count5;i++){
                    payload += '"';
                    payload += $(".input-group").eq(array5[i]).find(".en").html();
                    payload += '":"';
                    payload += $(".input-group").eq(array5[i]).find("input[type='text']").val();
                    payload += '"';
                    if(i!=count5-1){
                        payload += ',';
                    }
                }
                payload += '}';
            }
            payload += '}';
            $.ajax({
		        type: "POST",
		        url: "insert.jsp",  
		        data: {
		            adaptor:aid,
		            version:version,
		            payload:payload,
		            ip:$(".input-group").eq(3).find("input").val(),
		        },
		        success: function(msg){
		            if(msg==1){
                        alert("数据上报失败");
		            } else {
		                alert("数据上报成功");
                    }
		        }
	        });
        }
        update();
    });
});
function update(){
    if(aid==""){
        $("h4:last").hide();
        $(".update").html('');
        return;
    }else{
        $("h4:last").show();
        $.ajax({
	        type: "POST",
	        url: "update.jsp",  
	        data: {
	            aid:aid
	        },
	        success: function(msg){
                $(".update").html(msg);
	        }
        });
    }    
}
    </script>
</head>
<body>
    <div class="input-group">
        <span class="input-group-addon">适配器</span>
        <select class="form-control">
            <option value="">请选择适配器</option>
<%
String spl = "SELECT a.id,a.adaptor_id,b.id,b.name FROM gards_adaptor a left join gards_category_adaptor b on a.category_id=b.id where a.applied=1";
ResultSet rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("b.id")%>" aid="<%=rs.getString("a.id")%>" cname="<%=rs.getString("b.name")%>"><%=rs.getString("a.adaptor_id")%></option>
<%}
%>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-addon">适配器类型</span>
        <input type="text" class="form-control" readonly>
    </div>
    <div class="input-group">
        <span class="input-group-addon">版本</span>
        <input type="text" class="form-control">
    </div>
    <div class="input-group">
        <span class="input-group-addon">IP</span>
        <input type="text" class="form-control" value="
<%
if(request.getHeader("x-forwarded-for")==null){
    out.print(request.getRemoteAddr());
}else{
    out.print(request.getHeader("x-forwarded-for"));
}
%>
    " readonly>
    </div>
    <h4>位置 <input id="gis" type="checkbox"></h4>
    <div class="input-group">
        <label class="input-group-addon">经度</label>
        <input type="text" class="form-control">
    </div>
    <div class="input-group">
        <label class="input-group-addon">纬度</label>
        <input type="text" class="form-control">
    </div>
    <div class="input-group">
        <label class="input-group-addon">方向</label>
        <input type="text" class="form-control">
    </div>
    <div class="input-group">
        <label class="input-group-addon">速度</label>
        <input type="text" class="form-control">
    </div>
    <h4>状态节数据</h4>
<%
spl = "select def_status_id,name,category_id,value from gards_def_status where applied=1";
rs = statement.executeQuery(spl);
while (rs.next()){%>
    <div class="input-group" cat="station" cid="<%=rs.getString("category_id")%>">
        <label class="input-group-addon">
            <input type="checkbox">
            <span class="en"><%=rs.getString("def_status_id")%></span>
            <%=rs.getString("name")%>
        </label>
        <input type="text" class="form-control">
    <%if(rs.getString("value").length()!=0){%>
        <span class="input-group-addon"><%=rs.getString("value")%></span>
    <%}%>
    </div>
<%}%>
    <h4>事件节数据</h4>
<%
spl = "select def_event_id,name,category_id,value from gards_def_event where applied=1";
rs = statement.executeQuery(spl);
while (rs.next()){%>
    <div class="input-group" cat="event" cid="<%=rs.getString("category_id")%>">
        <label class="input-group-addon">
            <input type="checkbox">
            <span class="en"><%=rs.getString("def_event_id")%></span>
            <%=rs.getString("name")%>
        </label>
        <input type="text" class="form-control">
    <%if(rs.getString("value").length()!=0){%>
        <span class="input-group-addon"><%=rs.getString("value")%></span>
    <%}%>
    </div>
<%}%>
    <h4>报警节数据</h4>
<%
spl = "select def_alert_id,name,category_id,value from gards_def_alert where applied=1";
rs = statement.executeQuery(spl);
while (rs.next()){%>
    <div class="input-group" cat="alert" cid="<%=rs.getString("category_id")%>">
        <label class="input-group-addon">
            <input type="checkbox">
            <span class="en"><%=rs.getString("def_alert_id")%></span>
            <%=rs.getString("name")%>
        </label>
        <input type="text" class="form-control">
    <%if(rs.getString("value").length()!=0){%>
        <span class="input-group-addon"><%=rs.getString("value")%></span>
    <%}%>
    </div>
<%}%>
    <h4>设定节数据</h4>
<%
spl = "select def_set_id,name,category_id,value from gards_def_set where applied=1";
rs = statement.executeQuery(spl);
while (rs.next()){%>
    <div class="input-group" cat="set" cid="<%=rs.getString("category_id")%>">
        <label class="input-group-addon">
            <input type="checkbox">
            <span class="en"><%=rs.getString("def_set_id")%></span>
            <%=rs.getString("name")%>
        </label>
        <input type="text" class="form-control">
    <%if(rs.getString("value").length()!=0){%>
        <span class="input-group-addon"><%=rs.getString("value")%></span>
    <%}%>
    </div>
<%}%>
    <h4>请求节数据</h4>
<%
spl = "select def_req_id,name,value,category_id from gards_def_req where applied=1";
rs = statement.executeQuery(spl);
while (rs.next()){%>
    <div class="input-group" cat="req" cid="<%=rs.getString("category_id")%>">
        <label class="input-group-addon">
            <input type="checkbox">
            <span class="en"><%=rs.getString("def_req_id")%></span>
            <%=rs.getString("name")%>
        </label>
        <input type="text" class="form-control">
    <%if(rs.getString("value").length()!=0){%>
        <span class="input-group-addon"><%=rs.getString("value")%></span>
    <%}%>
    </div>
<%}%>
    <div class="buttons">
        <button type="button" class="btn btn-primary">提交</button>
        <button type="button" class="btn btn-danger">重置</button>
    </div>
    <h4>最新接受指令</h4>
    <div class="update"></div>
</body>
</html>
<%
rs.close();
statement.close();  
connection.close();
%>
