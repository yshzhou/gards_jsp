<%@ page contentType="text/html;charset=UTF-8"%>    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ include file="inc.jsp"%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style>
body { padding: 0 10px; }
body,th,td { font-size: 12px;}
.input-group { margin-top: 10px; }
.line {
    height: 0;
    border-top: 1px solid #DDD;
    overflow: hidden;
}
.main { display: none; }
.status h4 { float: left; }
.status #status {
    float: right;
    margin-top: 6px;
}
#mapn, #maph { height:220px; }
.JustGage { height: auto; }
#JustGage {
    width: 300px;
    height: 240px;
}
.canvas {
    padding-bottom: 10px;
    width: 300px;
    height: auto;
}
@media (min-width:640px) {
    .JustGage { float: left; }
    .canvas {
        float: left;
        padding-top: 50px;
    }
}
.cb { clear:both; }
.event tbody, .alter tbody, .set tbody { height: auto; }
    </style>
    <script src="jquery.min.js"></script>
    <script src="http://api.map.baidu.com/api?v=2.0&ak=26cxRIZuRqVLDBfbuWUCyfiF"></script>
<script>
//2011-7-25
(function(){        //闭包
function load_script(xyUrl, callback){
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = xyUrl;
    //借鉴了jQuery的script跨域方法
    script.onload = script.onreadystatechange = function(){
        if((!this.readyState || this.readyState === "loaded" || this.readyState === "complete")){
            callback && callback();
            // Handle memory leak in IE
            script.onload = script.onreadystatechange = null;
            if ( head && script.parentNode ) {
                head.removeChild( script );
            }
        }
    };
    // Use insertBefore instead of appendChild  to circumvent an IE6 bug.
    head.insertBefore( script, head.firstChild );
}
function translate(point,type,callback){
    var callbackName = 'cbk_' + Math.round(Math.random() * 10000);    //随机函数名
    var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from="+ type + "&to=4&x=" + point.lng + "&y=" + point.lat + "&callback=BMap.Convertor." + callbackName;
    //动态创建script标签
    load_script(xyUrl);
    BMap.Convertor[callbackName] = function(xyResult){
        delete BMap.Convertor[callbackName];    //调用完需要删除改函数
        var point = new BMap.Point(xyResult.x, xyResult.y);
        callback && callback(point);
    }
}
function transMore(points,type,callback){
	var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from=" + type + "&to=4&mode=1";
	var xs = [];
	var ys = [];
	var maxCnt = 20;//每次发送的最大个数
	var send = function(){
		var url = xyUrl + "&x=" + xs.join(",") + "&y=" + ys.join(",") + "&callback=callback";
	    //动态创建script标签
	    load_script(url);
		xs = [];
		ys = [];
	}
    for(var index in points){
		if(index % maxCnt == 0 && index != 0){
			send();
		}
    	xs.push(points[index].lng);
    	ys.push(points[index].lat);
		if(index == points.length - 1){
			send();
		}
    }
    
}
window.BMap = window.BMap || {};
BMap.Convertor = {};
BMap.Convertor.translate = translate;
BMap.Convertor.transMore = transMore;
})();
</script>
    <script src="raphael.2.1.0.min.js"></script>
    <script src="justgage.1.0.1.min.js"></script>
    <script src="Chart.js"></script>
    <script>
var BAIDU_TRANSMORE_COUNT=20;//参见changeMore.js源码，每次转换20个
var transMore_total=0;//全部需要转换的个数
var transMore_cnt=0;//已经转换了几次

var i = 0;
var aid = cid = '';
var refresh;
var mapNow,mapHis;
var pointArray = [];
$(document).ready(function(){
    $("#adaptor").change(function(){
        aid = $("#adaptor").val();
        if(aid==""){
            $(".main").hide();
            refresh = window.clearInterval(refresh);
        }else{
            cid = $("#adaptor").find("option:selected").attr("cid");
            $("#mapn").empty();
            $("#maph").empty();
            $("#status").html('<option value="">请选择状态</option>');
            $(".JustGage").empty();
            $(".canvas").empty();
            $("#EVNT tbody").empty();
            $("#ALT tbody").empty();
            $(".main").show();
            $.ajax({
	            type: "POST",
	            url: "viewReq1.jsp",
	            data: {
	                aid:aid,
                    cid:cid
	            },
                dataType: "json",
	            success: function(data){
				    if(data.mapn==''){
                        $(".now").hide();
                    }else{
                        $(".now").show();
                        var mapn = new Array();
                        mapn = data.mapn.split(",");
                        var mapnx = Number(mapn[0]);
                        var mapny = Number(mapn[1]);
                        if((mapnx==0)||(mapny==0)){
                            $(".now").hide();
                        }else{
                            mapNow = new BMap.Map("mapn");
                            var pointB,pointG;
                            pointG = new BMap.Point(mapnx,mapny);
                            BMap.Convertor.translate(pointG,0,translateCallback);
                        }
                    }
				    if(!data.maph){
                        $(".his").hide();
                    }else{
                        $(".his").show();
                        var maphx = new Array();
                        var maphy = new Array();
                        var count = 0;
                        for(i=0;i<data.maph.length;i++){
                            maph = new Array();
                            maph = data.maph[i].data.split(",");
                            if((Number(maph[0])!=0)&&(Number(maph[0])!=0)){
                                maphx[count] = Number(maph[0]);
                                maphy[count] = Number(maph[1]);
                                count++;
                            }
                        }
                        if(count!=0){
                            mapHis = new BMap.Map("maph");
                            var _pointArray = [];
                            for(i=0;i<count;i++){
                                _pointArray.push(new BMap.Point(maphx[i],maphy[i]));
                            }
                            transMore_total = count;
                            transMore_cnt = 0;
                            BMap.Convertor.transMore(_pointArray, 0, callback);
                        }
                    }
				    if(!data.STA){
                        $(".status").hide();
                    }else{
                        $(".status").show();
	                    for(i=0;i<data.STA.length;i++){
	                        $("#status").append('<option value="' + data.STA[i].id + '">' + data.STA[i].name + '</option>');
	                    }
				    }
				    if(!data.EVNT){
                        $(".event").hide();
                    }else{
                        $(".event").show();
	                    for(i=0;i<data.EVNT.length;i++){
	                        $("#EVNT tbody").append('<tr><td>' + data.EVNT[i].date_created + '</td><td>' + data.EVNT[i].name + '</td><td>' + data.EVNT[i].data + '</td><td>' + data.EVNT[i].value + '</td></tr>');
	                    }
				    }
				    if(!data.ALT){
                        $(".alter").hide();
                    }else{
                        $(".alter").show();
	                    for(i=0;i<data.ALT.length;i++){
	                        $("#ALT tbody").append('<tr><td>' + data.ALT[i].date_created + '</td><td>' + data.ALT[i].name + '</td><td>' + data.ALT[i].data + '</td><td>' + data.ALT[i].value + '</td></tr>');
	                    }
				    }
				    if(!data.SET){
                        $(".set").hide();
                    }else{
                        $(".set").show();
	                    for(i=0;i<data.SET.length;i++){
	                        $("#SET tbody").append('<tr><td>' + data.SET[i].date_created + '</td><td>' + data.SET[i].name + '</td><td>' + data.SET[i].data + '</td><td>' + data.SET[i].value + '</td></tr>');
	                    }
				    }
	            }
            });
            refresh = self.setInterval("f5()",10000)
        }
    });
    $("#status").change(function(){
        sc();
    });
});
function sc(){
    if($("#status").val()=="")return;
    $(".JustGage").empty();
    $(".canvas").empty();
    var title = $("#status").find("option:selected").html();
    $.ajax({
        type: "POST",
        url: "viewReq2.jsp",
        data: {
            aid:aid,
            id:$("#status").val()
        },
        dataType: "json",
        success: function(data){
            if(data.newS){
                $(".JustGage").html('<div id="JustGage"></div>');
                var jg = new JustGage({
                    id: 'JustGage',
                    value: data.newS,
                    min: 0,
                    max: Math.pow(10,(String(parseInt(data.newS)).length)),
                    title: title,
                    label: data.value
                });
            }
            if((data.oldS)&&(data.oldS.length>1)){
                $(".canvas").html("<canvas id=\"canvas\"></canvas>");
                var labelsChart = new Array(data.oldS.length);
                var dataChart = new Array(data.oldS.length);
                for(i=0;i<data.oldS.length;i++){
                    labelsChart[i] = i;
                    dataChart[i] = data.oldS[i].data;
                }
	            var lineChartData = {
		            labels : labelsChart,
		            datasets : [
			            {
				            label: "Label",
				            fillColor : "rgba(220,220,220,0.2)",
				            strokeColor : "rgba(220,220,220,1)",
				            pointColor : "rgba(220,220,220,1)",
				            pointStrokeColor : "#fff",
				            pointHighlightFill : "#fff",
				            pointHighlightStroke : "rgba(220,220,220,1)",
				            data : dataChart
			            }
		            ]
	            };
                var ctx = document.getElementById("canvas").getContext("2d");
	            var myLineChart = new Chart(ctx).Line(lineChartData, {
		            responsive: true
	            });
                myLineChart.clear()
	            myLineChart = new Chart(ctx).Line(lineChartData, {
		            responsive: true
	            });
            }
        }
    });
}
function f5(){
    if(aid==""){
        $(".main").hide();
    }else{
        $("#mapn").empty();
        $("#maph").empty();
        var eh = $("#EVNT tbody").height();
        $("#EVNT tbody").height(eh);
        $("#EVNT tbody").empty();
        var ah = $("#ALT tbody").height();
        $("#ALT tbody").height(ah);
        $("#ALT tbody").empty();
        $(".main").show();
        $.ajax({
            type: "POST",
            url: "viewReq1.jsp",
            data: {
                aid:aid,
                cid:cid
            },
            dataType: "json",
            success: function(data){
			    if(data.mapn==''){
                    $(".now").hide();
                }else{
                    $(".now").show();
                    var mapn = new Array();
                    mapn = data.mapn.split(",");
                    var mapnx = Number(mapn[0]);
                    var mapny = Number(mapn[1]);
                    if((mapnx==0)||(mapny==0)){
                        $(".now").hide();
                    }else{
                        mapNow = new BMap.Map("mapn");
                        var pointG = new BMap.Point(mapnx,mapny);
                        BMap.Convertor.translate(pointG,0,translateCallback);
                    }
                }
			    if(!data.maph){
                    $(".his").hide();
                }else{
                    $(".his").show();
                    var maphx = new Array();
                    var maphy = new Array();
                    var count = 0;
                    for(i=0;i<data.maph.length;i++){
                        var maph = new Array();
                        maph = data.maph[i].data.split(",");
                        if((Number(maph[0])!=0)&&(Number(maph[0])!=0)){
                            maphx[count] = Number(maph[0]);
                            maphy[count] = Number(maph[1]);
                            count++;
                        }
                    }
                    if(count!=0){
                        mapHis = new BMap.Map("maph");
                        var _pointArray = [];
                        for(i=0;i<count;i++){
                            _pointArray.push(new BMap.Point(maphx[i],maphy[i]));
                        }
                        transMore_total = count;
                        transMore_cnt = 0;
                        BMap.Convertor.transMore(_pointArray, 0, callback);
                    }
                }
			    if(!data.STA){
                    $(".status").hide();
                }else{
                    $(".status").show();
                    for(i=0;i<data.STA.length;i++){
                        $("#status").append('<option value="' + data.STA[i].id + '">' + data.STA[i].name + '</option>');
                    }
			    }
			    if(!data.EVNT){
                    $(".event").hide();
                }else{
                    $(".event").show();
                    for(i=0;i<data.EVNT.length;i++){
                        $("#EVNT tbody").append('<tr><td>' + data.EVNT[i].date_created + '</td><td>' + data.EVNT[i].name + '</td><td>' + data.EVNT[i].data + '</td><td>' + data.EVNT[i].value + '</td></tr>');
                    }
			    }
			    if(!data.ALT){
                    $(".alter").hide();
                }else{
                    $(".alter").show();
                    for(i=0;i<data.ALT.length;i++){
                        $("#ALT tbody").append('<tr><td>' + data.ALT[i].date_created + '</td><td>' + data.ALT[i].name + '</td><td>' + data.ALT[i].data + '</td><td>' + data.ALT[i].value + '</td></tr>');
                    }
			    }
			    if(!data.SET){
                    $(".set").hide();
                }else{
                    $(".set").show();
                    for(i=0;i<data.SET.length;i++){
                        $("#SET tbody").append('<tr><td>' + data.SET[i].date_created + '</td><td>' + data.SET[i].name + '</td><td>' + data.SET[i].data + '</td><td>' + data.SET[i].value + '</td></tr>');
                    }
			    }
            }
        });
    }
    var jh = $(".JustGage").height();
    $(".JustGage").height(jh);
    var ch = $(".canvas").height();
    $(".canvas").height(ch);
    sc();
}
translateCallback = function (point){
    mapNow.centerAndZoom(point, 15);
    var marker = new BMap.Marker(point);
    mapNow.addOverlay(marker);
    var nc = new BMap.NavigationControl();
    mapNow.addControl(nc);
}
function callback(xyResults){
	transMore_cnt+=1;
    var xyResult = null;
    for(var index in xyResults){
	    xyResult = xyResults[index];
	    if(xyResult.error != 0){continue;}//出错就直接返回;
	    var point = new BMap.Point(xyResult.x, xyResult.y);
        pointArray.push(point);
    }
	if (Math.ceil(transMore_total/BAIDU_TRANSMORE_COUNT) == transMore_cnt){
		displayTrace();
	}
}
function displayTrace(){
	if(pointArray.length > 1)
	{
        mapHis.centerAndZoom(pointArray[0], 15);
        var icons = new BMap.Icon("start.png", new BMap.Size(29,32));
        var markers = new BMap.Marker(pointArray[0],{icon:icons});
        mapHis.addOverlay(markers);
        var icone = new BMap.Icon("end.png", new BMap.Size(29,32));
        var markere = new BMap.Marker(pointArray[pointArray.length-1],{icon:icone});
        mapHis.addOverlay(markere);
        var polyline = new BMap.Polyline(pointArray,{strokeColor:"red", strokeWeight:2, strokeOpacity:0.5});
        mapHis.addOverlay(polyline);
        var nc = new BMap.NavigationControl();
        mapHis.addControl(nc);
	}
}
    </script>
</head>
<body>
    <div class="input-group">
        <span class="input-group-addon">适配器</span>
        <select id="adaptor" class="form-control">
            <option value="">请选择适配器</option>
<%
String spl = "SELECT id,adaptor_id,category_id FROM gards_adaptor where applied=1";
ResultSet rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("id")%>" cid="<%=rs.getString("category_id")%>"><%=rs.getString("adaptor_id")%></option>
<%}
%>
        </select>
    </div>
    <div class="main">
        <div class="now">
            <h4>当前位置</h4>
            <div id="mapn"></div>
        </div>
        <div class="his">
            <h4>历史轨迹</h4>
            <div id="maph"></div>
        </div>
        <div class="status">
            <h4>状态节数据</h4>
            <select id="status"></select>
            <div class="cb"></div>
            <div class="JustGage"></div>
            <div class="canvas"></div>
            <div class="cb"></div>
        </div>
        <div class="event">
            <div class="line"></div>
            <h4>事件节数据</h4>
            <table id="EVNT" class="table">
                <thead>
                    <tr>
                        <th>日期时间</th>
                        <th>事件</th>
                        <th>数值</th>
                        <th>单位</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="alter">
            <div class="line"></div>
            <h4>报警节数据</h4>
            <table id="ALT" class="table">
                <thead>
                    <tr>
                        <th>日期时间</th>
                        <th>报警</th>
                        <th>数值</th>
                        <th>单位</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="set">
            <div class="line"></div>
            <h4>设定节数据</h4>
            <table id="SET" class="table">
                <thead>
                    <tr>
                        <th>日期时间</th>
                        <th>设定</th>
                        <th>数值</th>
                        <th>单位</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</body>
</html>
