<%@ page contentType="text/html;charset=UTF-8"%>    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ include file="inc.jsp"%>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>App</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style>
body { padding: 0 10px; }
.input-group { margin-top: 10px; }
.en { display:none; }
.buttons {
    padding: 10px 0;
    text-align: right;
}
    </style>
    <script src="jquery.min.js"></script>
    <script>
$(document).ready(function(){
    $("h4:last").hide();
    var i = 0;
    var app = $(".app");
    $(".input-group").eq(0).find("select").change(function(){
        app.empty();
        if($(this).val()!=''){
            $.ajax({
		        type: "POST",
		        url: "appReq1.jsp",  
		        data: {
		            cid:$(this).val()
		        },
                dataType: "json",
		        success: function(data){
                    for(i=0;i<data.length;i++){
                        if(data[i].value==''){
                            app.append('<div class="input-group"><label class="input-group-addon"><input type="checkbox"><span class="en">' + data[i].action_id + '</span> ' + data[i].name + '</label><input type="text" class="form-control"></div>');
                        }else{
                            app.append('<div class="input-group"><label class="input-group-addon"><input type="checkbox"><span class="en">' + data[i].action_id + '</span> ' + data[i].name + '</label><input type="text" class="form-control"><span class="input-group-addon">' + data[i].value + '</span></div>');
                        }
                    }
		        }
	        });
        }
        update();
    });
    $(".input-group").eq(1).find("select").change(function(){
        update();
    });
    $(".btn-danger").click(function(){
        for(i=2;i<$(".input-group").size();i++){
            $(".input-group").eq(i).find("input[type='text']").val("");
        }
    });
    $(".btn-primary").click(function(){
        var version = $(".input-group").eq(2).find("input[type='text']").val();
        if($(".input-group").eq(0).find("select").val()==""){
            alert("请选择适配器");
            return;
        }else if($(".input-group").eq(1).find("option:selected").val()==""){
            alert("请选择应用系统");
            return;
        }else{
            var count = 0;
            var array = new Array();
            for(i=2;i<$(".input-group").size();i++){
                if(($(".input-group").eq(i).find("label").find("input").prop("checked")!="")){
                    array[count]=i;
                    count++;
                }
            }
            if(count==0){
                alert("请选择数据输入");
                return;
            }
            var payload = '{"ACT":{';
            for(i=0;i<count;i++){
                payload += '"';
                payload += $(".input-group").eq(array[i]).find(".en").html();
                payload += '":"';
                payload += $(".input-group").eq(array[i]).find("input[type='text']").val();
                payload += '"';
                if(i!=count-1){
                    payload += ',';
                }
            }
            payload += '}}';
            $.ajax({
		        type: "POST",
		        url: "appIns1.jsp",  
		        data: {
		            aid1:$(".input-group").eq(0).find("option:selected").attr("aid"),
		            aid2:$(".input-group").eq(1).find("option:selected").val(),
		            payload:payload
		        },
		        success: function(msg){
		            if(msg==1){
                        alert("数据上报失败");
		            } else {
		                alert("数据上报成功");
                    }
		        }
	        });
        }
        update();
    });
});
function update(){
    if($(".input-group").eq(1).find("option:selected").val()==""){
        $("h4:last").hide();
        $(".update").html('');
        return;
    }else{
        $("h4:last").show();
        $.ajax({
	        type: "POST",
	        url: "appUpd1.jsp",  
	        data: {
	            aid:$(".input-group").eq(1).find("option:selected").val()
	        },
	        success: function(msg){
                $(".update").html(msg);
	        }
        });
    }    
}
    </script>
</head>
<body>
    <div class="input-group">
        <span class="input-group-addon">适配器</span>
        <select class="form-control">
            <option value="">请选择适配器</option>
<%
String spl = "SELECT a.id,a.adaptor_id,b.id FROM gards_adaptor a left join gards_category_adaptor b on a.category_id=b.id where a.applied=1";
ResultSet rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("b.id")%>" aid="<%=rs.getString("a.id")%>"><%=rs.getString("a.adaptor_id")%></option>
<%}
%>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-addon">应用系统</span>
        <select class="form-control">
            <option value="">请选择应用系统</option>
<%
spl = "SELECT id,name FROM gards_application";
rs = statement.executeQuery(spl);
while (rs.next()){%>
            <option value="<%=rs.getString("id")%>"><%=rs.getString("name")%></option>
<%}
%>
        </select>
    </div>
    <div class="app"></div>
    <div class="buttons">
        <button type="button" class="btn btn-primary">提交</button>
        <button type="button" class="btn btn-danger">重置</button>
    </div>
    <h4>最新接受指令</h4>
    <div class="update"></div>
</body>
</html>
<%
rs.close();
statement.close();  
connection.close();
%>
